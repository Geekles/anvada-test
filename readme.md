## About

Before the deployment of this pplication on your computer, you need to make sure that your environment is ready. Check it first:

- Laravel 5.8
- Composer
- PHP >= 7.2
- Mysql 5.7

If all those specifications are respected, you can proceed.

## Installation

First, you have to clone this project using this command:

```
git clone https://gitlab.com/Geekles/anvada-test.git
```

Next, do this to install some dependencies located in **composer.json**

```
composer install
```

For the connection parameters to database, you can use those already present in the **.env** file or change them according to your own settings.

Here is my settings

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=anvada_db
DB_USERNAME=root
DB_PASSWORD=
```

If everything is ok, then you can execute this command in the terminal

```
php artisan serve
```

After this, you can use the endpoints.

Good luck :)