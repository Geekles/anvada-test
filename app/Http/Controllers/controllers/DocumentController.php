<?php

namespace App\Http\Controllers\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Document;
use App\Http\Resources\DocumentResource;
use App\Enums\StatusEnum;
use Illuminate\Http\Response;
use Webpatser\Uuid\Uuid;
use \Illuminate\Pagination\Paginator;

class DocumentController extends Controller
{

    /**
     * get document's list
     * 
     * @param int $page
     * @param int $perPage
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->query('page');
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $docs = Document::paginate($request->query('perPage'));
        return Response(array('document' => DocumentResource::collection($docs), 'pagination' => [
            'page' => $docs->currentPage(),
            'perPage' => $docs->perPage(),
            'total' => $docs->total()
        ]), 200);
    }

    /**
     * Save document in database
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $doc = new Document; 
        $doc->id = Uuid::generate()->string;
        $doc->status = $request->input('document')['status'];
        $doc->payload = json_encode($request->input('document')['payload']);
        if($doc->save()) {
            return Response(array('document' => new DocumentResource($doc)), 200);
        }
    }

    /**
     * get a document by his id
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $doc = Document::find($id);
        return Response(array('document' => new DocumentResource($doc)), 200);
    }

    /**
     * Update a document in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $doc = Document::findOrFail($id);
        if($doc->status->getValue() == "draft") {
            $doc->status = $doc->status->getValue();
            $doc->payload = json_encode($request->input('document')['payload']) == null ? $doc->payload : json_encode($request->input('document')['payload']);
            $doc->save();
            return Response(array('document' => new DocumentResource($doc)), 200);
        } else {
            return Response(array('message' => "Already published, you can't modify this document."), 400);
        }
        
    }

    /**
     * publish a document in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function publish(Request $request, $id)
    {
        $doc = Document::find($id);
        $doc->status = 'published';
        $doc->save();
        return Response(array('document' => new DocumentResource($doc)), 200);
    }
}
