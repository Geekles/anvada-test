<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Enum\Laravel\HasEnums;

use App\Enums\StatusEnum;

class Document extends Model
{
    use HasEnums;

    public $incrementing = false;   

    protected $fillable = ['id', 'status', 'payload'];

    protected $keyType = 'string';

    protected $enums = [
        'status' => StatusEnum::class,
    ];

    protected $casts = [
        'payload' => 'array',
    ];

    /**
     * Get the document's status.
     *
     * @param  StatusEnum  $value
     * @return StatusEnum
     */
    public function getStatusAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Get the document's payload.
     *
     * @param  string  $value
     * @return string
     */
    public function getPayloadAttribute($value) {
        return ucfirst($value);
    }

    /**
     * Set the payload's json value
     *
     * @param  string  $value
     * @return void
     */
    public function setPayloadAttribute($value)
    {
        $this->attributes['payload'] = $value;
    }
}
