<?php

namespace App\Enums;

use Spatie\Enum\Enum;

class StatusEnum extends Enum
{
    public static function draft(): StatusEnum
    {
        return new class() extends StatusEnum {
            public function getValue(): string
            {
                return 'draft';
            }
            public function getIndex(): int
            {
                return 10;
            }
        };
    }
    
    public static function published(): StatusEnum
    {
        return new class() extends StatusEnum {
            public function getValue(): string
            {
                return 'published';
            }
            public function getIndex(): int
            {
                return 20;
            }
        };
    }
}
