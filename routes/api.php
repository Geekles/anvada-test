<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function() {
    Route::post('document', 'controllers\DocumentController@store')->name('create');
    Route::get('document/{id}', 'controllers\DocumentController@show')->name('getone');
    Route::patch('document/{id}', 'controllers\DocumentController@update')->name('update');
    Route::post('document/{id}/publish', 'controllers\DocumentController@publish')->name('publish');
    Route::get('document', 'controllers\DocumentController@index')->name('all');
});